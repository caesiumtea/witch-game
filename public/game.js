"use strict";

/* DOM Setup */

const game = document.getElementById("game");
const textbox = document.getElementById("textbox");
const title = document.getElementById("titlebox");
const topMenu = document.getElementById("topMenu");
const openTopMenuBtn = document.getElementById("openTopMenuBtn");
const dialog = document.getElementById("dialog");

/* Images */

const bgs = {
    "hearth": {
      "path": "img/bg-hearth.png"
    },
    // "places": {
    //   "path": "img/bg-places.png"
    // },
    "woods": {
      "path": "img/bg-woods.png" //TODO
    },
    "journal": {
      "path": "img/bg-journal.png" //TODO
    }
  };

/* Script */

const script = {
    "intro1": {
        "text": "I can't believe it's the last week of my study abroad trip already!",
        "next": "intro2"
    },
    "intro2": {
        "text": "intro p 2",
        "next": null
    },
    "ending": {
        "text": "",
        "next": null
    }
}

/* UI Functions */

function showTitle() {
    title.style.display = "flex";
}

function hideTitle() {
    title.style.display = "none";
}

function showTextbox() {
    textbox.style.display = "unset";
}

function hideTextbox() {
    textbox.style.display = "none";
}

function showTopMenu() {
    topMenu.style.display = "flex";
    openTopMenuBtn.style.display = "none";

}

function hideTopMenu() {
    topMenu.style.display = "none";
    openTopMenuBtn.style.display = "unset";
}

function showText(text) {
    let para = document.createElement("p");
    para.appendChild(document.createTextNode(text));
    dialog.replaceWith(para);
}

/* Progression */

// find id in script object and return its text property 
function getText(id) {
    return script[id].text;
}

// decide what to do after continue button is pressed
function next() {
    return false;
}

// clumsy attempt at advancing text to next line
function follow(beat) {
    if (beat.next !== null) {
        beat = beat.next;
        showText(getText(beat));
        return beat;
    }
}

/**********************************
/ START
**********************************/

function start() {
    hideTitle();

    let beat = "intro1";
    let text = getText(beat);
    showText(text);

    
}