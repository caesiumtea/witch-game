# unnamed witch game (WIP)
a narrative game about a witch's last week of study abroad. originally conceived for Ludum Dare 50, on the theme "delay the inevitable". still very much a work in progress, and not actually a playable game yet!

[live WIP on GitLab Pages](https://caesiumtea.gitlab.io/witch-game/)

my first attempt at creating this game during Ludum Dare can be found on [GitHub](https://github.com/caesiumtea/ludum-dare-50/). this repo is a remake from the ground up. because i was just trying to learn JS as i went along and had absolutely no idea what i was doing when i started. well i mean, i'm still mostly just learning JS as i go, but i'm starting from a little better of a standing this time.

[design document and brainstorm](https://github.com/caesiumtea/entropic-garden/blob/main/entropic-garden/notes/witch-game.md)

all art and writing are by me as well as the code! i will probably make the code open source eventually, but keep the art and story proprietary.